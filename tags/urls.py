from django.urls import path

from tags.views import (
    TagListView,
    TagCreateView,
    TagDetailView,
    TagDeleteView,
    TagUpdateView,
)

urlpatterns = [
    path("", TagListView.as_view(), name="tags_list"),
    path("new/", TagCreateView.as_view(), name="tags_create"),
    path("<int:pk>", TagDetailView.as_view(), name="tags_detail"),
    path("<int:pk>/delete/", TagDeleteView.as_view(), name="tags_delete"),
    path("<int:pk>/edit/", TagUpdateView.as_view(), name="tags_update"),
]
