# Generated by Django 4.0.3 on 2022-04-21 00:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0013_step_food_item'),
    ]

    operations = [
        migrations.AlterField(
            model_name='step',
            name='food_item',
            field=models.ManyToManyField(blank=True, to='recipes.fooditem'),
        ),
    ]
