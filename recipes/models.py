from email import charset
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from django.conf import settings
from django.forms import CharField
from psycopg2 import IntegrityError

# from django.forms import CharField

USER_MODEL = settings.AUTH_USER_MODEL


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.ForeignKey(
        USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    servings = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.name + " by " + str(self.author)


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name + " " + self.abbreviation


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField(
        validators=[MinValueValidator(0), MaxValueValidator(20)]
    )
    recipe = models.ForeignKey(
        "Recipe", related_name="ingredients", on_delete=models.CASCADE
    )
    measure = models.ForeignKey(
        "Measure", related_name="measure", on_delete=models.PROTECT
    )
    food_item = models.ForeignKey(
        "FoodItem", related_name="fooditem", on_delete=models.PROTECT
    )

    def __str__(self):
        return (
            str(self.amount)
            + " "
            + str(self.measure)
            + " "
            + str(self.food_item)
        )


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    order = models.PositiveSmallIntegerField()
    directions = models.TextField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", blank=True)

    def __str__(self):
        return str(self.order) + "." + self.directions


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
    recipe = models.ForeignKey(
        "Recipe", related_name="ratings", on_delete=models.CASCADE
    )


class ShoppingItem(models.Model):
    user = models.ForeignKey(
        USER_MODEL, related_name="shopping_items", on_delete=models.CASCADE
    )
    food_item = models.ForeignKey("FoodItem", on_delete=models.PROTECT)


# Create your models here.
